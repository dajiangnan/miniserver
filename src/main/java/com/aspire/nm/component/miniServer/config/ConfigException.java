package com.aspire.nm.component.miniServer.config;


public class ConfigException extends Exception {
    
    public ConfigException(){
        super();
    }
    public ConfigException(String message){
        super(message);
    }
    private static final long serialVersionUID = 2492923761541712979L;
}
