package com.aspire.nm.component.miniServer.plugin;


public interface Cacher<T> extends Iplugin{
    public boolean put(String key,T t,int time);
    public T get(String key);
}
