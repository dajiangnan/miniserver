package com.aspire.nm.component.miniServer.status;

import java.lang.reflect.Method;


public class RoutingMethodKey {

    

    public static String getMethodInfo(Method method,boolean simple){
        String methodInfo = "";
        
        String methodName = method.getName();
        Class<?> returnTypes = method.getReturnType();
        Class<?>[] parameterTypes = method.getParameterTypes();
        
        if(simple){
            String simpleReturnTypes = returnTypes.toString();
            if(simpleReturnTypes.lastIndexOf(".") != -1){
                simpleReturnTypes = simpleReturnTypes.substring(simpleReturnTypes.lastIndexOf(".")+1);
            }
            methodInfo += simpleReturnTypes + " " +methodName+"(";
        }else{
            methodInfo += returnTypes + " " +methodName+"(";
        }
        
        for(int i = 0 ;i < parameterTypes.length ; i ++){
            String parameterTypesString =  parameterTypes[i].toString();
            if(simple){
                if(parameterTypesString.lastIndexOf(".") != -1){
                    parameterTypesString = parameterTypesString.substring(parameterTypesString.lastIndexOf(".")+1);
                }
            }
            if(i == parameterTypes.length - 1){
                methodInfo += parameterTypesString;
            }else{
                methodInfo += parameterTypesString+",";
            }   
        }
        methodInfo += ")";
        return methodInfo;
    }
}
