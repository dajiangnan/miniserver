package com.aspire.nm.component.miniServer.render;


public class NativeRenderFactory  implements RenderProvider {
    @Override
    public Render getRender() {
        return new NativeRender();
    }
}
