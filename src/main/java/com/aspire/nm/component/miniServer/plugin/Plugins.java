package com.aspire.nm.component.miniServer.plugin;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.aspire.nm.component.miniServer.aop.Around;
import com.aspire.nm.component.miniServer.plugin.defaultImpl.CommonIfCacher;
import com.aspire.nm.component.miniServer.plugin.defaultImpl.CommonResourceCacher;
import com.aspire.nm.component.miniServer.plugin.defaultImpl.CommonSessionCacher;


public class Plugins {
    
    private Map<String,Iplugin> pluginMap = new HashMap<String,Iplugin>();
    private Around around;
    
    public Plugins(){
        pluginMap.put("ifCacher", new CommonIfCacher());
        pluginMap.put("sessionCacher", new CommonSessionCacher());
        pluginMap.put("resourceCacher", new CommonResourceCacher());
    }
    
    
    public void start(){
        Iterator<Map.Entry<String, Iplugin>> it = pluginMap.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, Iplugin> entry = it.next();
            entry.getValue().start();
        }
    }
    public void release(){
        Iterator<Map.Entry<String, Iplugin>> it = pluginMap.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, Iplugin> entry = it.next();
            entry.getValue().release();
        }
    }
    
    
    
    
    
    
    public Around getAround() {
        return around;
    }
    public void setAround(Around around) {
        this.around = around;
    }
    public IfCacher getIfCacher() {
        return (IfCacher)pluginMap.get("ifCacher");
    }
    public void setIfCacher(IfCacher ifCacher) {
        pluginMap.put("ifCacher", ifCacher);
    }
    public SessionCacher getSessionCacher() {
        return (SessionCacher)pluginMap.get("sessionCacher");
    }
    public void setSessionCacher(SessionCacher sessionCacher) {
        pluginMap.put("sessionCacher", sessionCacher);
    }
    public ResourceCacher getResourceCacher() {
        return (ResourceCacher)pluginMap.get("resourceCacher");
    }
    public void setResourceCacher(ResourceCacher resourceCacher) {
        pluginMap.put("resourceCacher", resourceCacher);
    }
}
