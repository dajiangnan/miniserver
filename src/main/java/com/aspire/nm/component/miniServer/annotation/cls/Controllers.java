package com.aspire.nm.component.miniServer.annotation.cls;


import java.lang.annotation.Documented; 
import java.lang.annotation.ElementType; 
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy; 
import java.lang.annotation.Target; 

  
@Target(ElementType.TYPE) 
@Retention(RetentionPolicy.RUNTIME) 
@Documented 
public @interface Controllers { 
	  String path() default "";
	  int timeOut() default 0;
	  String allowIpsPattern() default "";
	  MethodType methodType() default MethodType.BOTH;
      public enum MethodType {  
            GET,POST,BOTH;
        }
} 
