package com.aspire.nm.component.miniServer.plugin.defaultImpl;

import com.aspire.nm.component.miniServer.plugin.IfCacher;
import com.aspire.nm.component.miniServer.protocol.Response;
import com.aspire.nm.component.util.CacheUtil;


public class CommonIfCacher implements IfCacher {

    private CacheUtil cacheUtil;
    public CommonIfCacher(){
        cacheUtil = new CacheUtil();
    }
    
    @Override
    public boolean put(String key, Response obj, int time) {
        return cacheUtil.put(key, obj, (long)time);
    }

    @Override
    public Response get(String key) {
        return (Response)cacheUtil.get(key);
    }

    @Override
    public void start() {
    }

    @Override
    public void release() {
    }

}
