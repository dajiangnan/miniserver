package com.aspire.nm.component.miniServer.aop;

import java.lang.reflect.Method;

import com.aspire.nm.component.miniServer.Controll;


public interface Around {

    /**
     * 每个controll方法执行前执行before方法
     * @param controll controll本身
     * @param method   method方法本身
     * @param params   执行参数
     */
    public void before(Controll controll,Method method,Object[] params);
    
    /**
     * 每个controll方法执行后执行after方法
     * @param controll controll本身
     * @param method   method方法本身
     * @param params   执行参数
     * @param returnObject 返回值
     */
    public void after(Controll controll,Method method,Object[] params,Object returnObject);
}
