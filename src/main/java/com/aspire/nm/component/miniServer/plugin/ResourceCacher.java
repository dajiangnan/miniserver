package com.aspire.nm.component.miniServer.plugin;



public interface ResourceCacher extends Cacher<byte[]>{

    public boolean put(String key,byte[] bytes,int time);
    public byte[] get(String key);
}
