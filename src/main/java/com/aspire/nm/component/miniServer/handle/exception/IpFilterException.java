package com.aspire.nm.component.miniServer.handle.exception;


public class IpFilterException extends Exception {
    
    public IpFilterException(){
        super();
    }
    public IpFilterException(String message){
        super(message);
    }
    private static final long serialVersionUID = 2492923761542712979L;
}
