package com.aspire.nm.component.miniServer.config;

import java.util.HashMap;
import java.util.Map;

import com.aspire.nm.component.util.ConstantConfig;



public class DefaultDefine {

    
    private static final Map<String,String> defaultConfig = new HashMap<String,String>();
    
    private static final String configDefaultPath = "config/properties/configDefault.properties";
    
    static{
        defaultConfig.put("serverPort", ConstantConfig.getPropertiesValue(configDefaultPath,"serverPort"));
        
        defaultConfig.put("corePoolSize", ConstantConfig.getPropertiesValue(configDefaultPath,"corePoolSize"));
        defaultConfig.put("maximumPoolSize", ConstantConfig.getPropertiesValue(configDefaultPath,"maximumPoolSize"));
        defaultConfig.put("workQueueSize", ConstantConfig.getPropertiesValue(configDefaultPath,"workQueueSize"));
        
        defaultConfig.put("sessionExpireSec", ConstantConfig.getPropertiesValue(configDefaultPath,"sessionExpireSec"));
        defaultConfig.put("resourceExpireSec", ConstantConfig.getPropertiesValue(configDefaultPath,"resourceExpireSec"));
        
        defaultConfig.put("absoluteWebappDir", ConstantConfig.getPropertiesValue(configDefaultPath,"absoluteWebappDir"));
        
        defaultConfig.put("forbiddenSourcePattern", ConstantConfig.getPropertiesValue(configDefaultPath,"forbiddenSourcePattern"));
        defaultConfig.put("allowIpsPattern", ConstantConfig.getPropertiesValue(configDefaultPath,"allowIpsPattern"));
        defaultConfig.put("adminAllowIpsPattern", ConstantConfig.getPropertiesValue(configDefaultPath,"adminAllowIpsPattern"));
        
        defaultConfig.put("clientEncode", ConstantConfig.getPropertiesValue(configDefaultPath,"clientEncode"));
        defaultConfig.put("serverEncode", ConstantConfig.getPropertiesValue(configDefaultPath,"serverEncode"));
        
        defaultConfig.put("renderFactoryClassName", ConstantConfig.getPropertiesValue(configDefaultPath,"renderFactoryClassName"));
        defaultConfig.put("jsonFormatErr", ConstantConfig.getPropertiesValue(configDefaultPath,"jsonFormatErr"));
        defaultConfig.put("annotationValueConfigFile", ConstantConfig.getPropertiesValue(configDefaultPath,"annotationValueConfigFile"));
        defaultConfig.put("accessLogInLine",ConstantConfig.getPropertiesValue(configDefaultPath,"accessLogInLine"));
        defaultConfig.put("httpRespHeaderDefineFile", ConstantConfig.getPropertiesValue(configDefaultPath,"httpRespHeaderDefineFile"));
        
    }
    
    
    public static final String getDefault(String key){
        if(defaultConfig.containsKey(key)){
            return defaultConfig.get(key);
        }
        throw new RuntimeException("can not get default value by key = " + key);
    }
}
