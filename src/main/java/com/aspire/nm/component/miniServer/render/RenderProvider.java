package com.aspire.nm.component.miniServer.render;


public interface RenderProvider {
    public Render getRender();
}
