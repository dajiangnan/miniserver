package com.aspire.nm.component.miniServer.plugin.defaultImpl;

import com.aspire.nm.component.util.CacheUtil;
import com.aspire.nm.component.miniServer.plugin.ResourceCacher;


public class CommonResourceCacher implements ResourceCacher {

    private CacheUtil cacheUtil;
    public CommonResourceCacher(){
        cacheUtil = new CacheUtil();
    }
    
    @Override
    public byte[] get(String key) {
        return (byte[])cacheUtil.get(key);
    }

    @Override
    public boolean put(String key, byte[] bytes, int time) {
        return cacheUtil.put(key, bytes, (long)time);
    }

    @Override
    public void start() {
    }

    @Override
    public void release() {
    }

}
