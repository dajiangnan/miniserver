package com.aspire.nm.component.miniServer.handle.exception;


public class JsonFormatErrException extends Exception {
    
    public JsonFormatErrException(){
        super();
    }
    public JsonFormatErrException(String message){
        super(message);
    }
    private static final long serialVersionUID = 2492923761542712979L;
}
