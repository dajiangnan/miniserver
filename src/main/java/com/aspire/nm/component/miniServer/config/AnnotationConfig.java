package com.aspire.nm.component.miniServer.config;

import java.util.HashMap;
import java.util.Map;

import com.aspire.nm.component.util.ConstantConfig;
import org.apache.commons.lang3.StringUtils;



public class AnnotationConfig {

    private Config config;
    
    private Map<String,Map<String,Object>> annotationConfig;

    
    public AnnotationConfig(Config config){
        this.config = config;
        annotationConfig = new HashMap<String,Map<String,Object>>();
    }
    
    public void put(String className,String annotationName,Object object)throws ConfigException{
        if(annotationConfig.get(className) == null){
            annotationConfig.put(className, new HashMap<String,Object>());
        }
        if(object instanceof String && ((String)object).startsWith("$")){
            String value = ConstantConfig.getPropertiesValue(config.getAnnotationValueConfigFile(), ((String)object).substring(1));
            if(StringUtils.isEmpty(value)){
                throw new ConfigException("can get value from annotation config , key = " + object);
            }else{
                if(annotationName.equals("timeOut")){
                    try{
                        annotationConfig.get(className).put(annotationName, Integer.parseInt(value));
                    }catch(NumberFormatException e){
                        throw new ConfigException("timeOut annotation config must be number , key = " + object);
                    }
                }else{
                    annotationConfig.get(className).put(annotationName, value);
                }
                
            }
        }else{
            annotationConfig.get(className).put(annotationName, object);
        }
        
    }
    
    
    public Object get(String className,String annotationName){
        if(annotationConfig.get(className) == null){
            return null;
        }else{
            return annotationConfig.get(className).get(annotationName);
        }
    }
}
