package com.aspire.nm.component.miniServer.status;

import java.util.concurrent.atomic.AtomicLong;


public class Apath {

    public String methodInfo;
    
    public String path;
    public String getPost;
    public String cacheTime;
    public String timeOut;
    public String desc;
    public String allowsIps;
    
    private AtomicLong count = new AtomicLong(0);
    private AtomicLong count_200 = new AtomicLong(0);
    private AtomicLong last_200 = new AtomicLong(0); 
    private AtomicLong count_403 = new AtomicLong(0);
    private AtomicLong count_404 = new AtomicLong(0);
    private AtomicLong count_500 = new AtomicLong(0);
    private AtomicLong count_504 = new AtomicLong(0);
    
    
    public long getarg(){
        if(count_200.get() == 0)return 0;
        return last_200.get()/count_200.get();
    }
    
    
    public AtomicLong getLast_200() {
    
        return last_200;
    }



    
    public void setLast_200(AtomicLong last_200) {
    
        this.last_200 = last_200;
    }



    public AtomicLong getCount() {
    
        return count;
    }


    
    public void setCount(AtomicLong count) {
    
        this.count = count;
    }


    public AtomicLong getCount_200() {
    
        return count_200;
    }

    
    public void setCount_200(AtomicLong count_200) {
    
        this.count_200 = count_200;
    }

    
    public AtomicLong getCount_403() {
    
        return count_403;
    }

    
    public void setCount_403(AtomicLong count_403) {
    
        this.count_403 = count_403;
    }

    
    public AtomicLong getCount_404() {
    
        return count_404;
    }

    
    public void setCount_404(AtomicLong count_404) {
    
        this.count_404 = count_404;
    }

    
    public AtomicLong getCount_500() {
    
        return count_500;
    }

    
    public void setCount_500(AtomicLong count_500) {
    
        this.count_500 = count_500;
    }

    
    public AtomicLong getCount_504() {
    
        return count_504;
    }

    
    public void setCount_504(AtomicLong count_504) {
    
        this.count_504 = count_504;
    }

    public String getMethodInfo() {
    
        return methodInfo;
    }
    
    public void setMethodInfo(String methodInfo) {
    
        this.methodInfo = methodInfo;
    }
    
    public String getPath() {
    
        return path;
    }
    
    public void setPath(String path) {
    
        this.path = path;
    }
    
    public String getGetPost() {
    
        return getPost;
    }
    
    public void setGetPost(String getPost) {
    
        this.getPost = getPost;
    }
    
    public String getCacheTime() {
    
        return cacheTime;
    }
    
    public void setCacheTime(String cacheTime) {
    
        this.cacheTime = cacheTime;
    }
    
    public String getTimeOut() {
    
        return timeOut;
    }
    
    public void setTimeOut(String timeOut) {
    
        this.timeOut = timeOut;
    }
    
    public String getDesc() {
    
        return desc;
    }
    
    public void setDesc(String desc) {
    
        this.desc = desc;
    }


    
    public String getAllowsIps() {
    
        return allowsIps;
    }


    
    public void setAllowsIps(String allowsIps) {
    
        this.allowsIps = allowsIps;
    }
    
    
}
