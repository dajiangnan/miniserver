package com.aspire.nm.component.miniServer.handle.exception;


public class ForbiddenSourceFilterException extends Exception {
    
    public ForbiddenSourceFilterException(){
        super();
    }
    public ForbiddenSourceFilterException(String message){
        super(message);
    }
    private static final long serialVersionUID = 2492923761542712979L;
}
