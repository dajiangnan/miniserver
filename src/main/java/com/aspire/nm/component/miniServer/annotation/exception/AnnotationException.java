package com.aspire.nm.component.miniServer.annotation.exception;


public class AnnotationException extends Exception {
    
    public AnnotationException(){
        super();
    }
    public AnnotationException(String message){
        super(message);
    }
    private static final long serialVersionUID = 2492923761541712979L;
}
