package com.aspire.nm.component.miniServer.render;


public class RenderException extends Exception {
    
    public RenderException(){
        super();
    }
    public RenderException(String message){
        super(message);
    }
    private static final long serialVersionUID = 2422923761542712979L;
}
