package com.aspire.nm.component.miniServer.protocol;

import java.io.Serializable;


public class Response implements Serializable{

    private static final long serialVersionUID = -6589038629753202855L;
    
    private int httpRespStatus;
	private String httpRespInfo;
	private byte[] contentBytes;
	private String contentType;
	private String contentDisposition;
	private String location;
	private long ifcacherExpireTime;
	
	
	
	public long getIfcacherExpireTime() {
		return ifcacherExpireTime;
	}
	public void setIfcacherExpireTime(long ifcacherExpireTime) {
		this.ifcacherExpireTime = ifcacherExpireTime;
	}
	public String getLocation() {
        return location;
    }
    public void setLocation(String location) {
        this.location = location;
    }
    public String getContentDisposition() {
		return contentDisposition;
	}
	public void setContentDisposition(String contentDisposition) {
		this.contentDisposition = contentDisposition;
	}
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	public int getHttpRespStatus() {
		return httpRespStatus;
	}
	public void setHttpRespStatus(int httpRespStatus) {
		this.httpRespStatus = httpRespStatus;
	}
	public String getHttpRespInfo() {
		return httpRespInfo;
	}
	public void setHttpRespInfo(String httpRespInfo) {
		this.httpRespInfo = httpRespInfo;
	}
	public byte[] getContentBytes() {
		return contentBytes;
	}
	public void setContentBytes(byte[] contentBytes) {
		this.contentBytes = contentBytes;
	}
}
