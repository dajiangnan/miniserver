package com.aspire.nm.component.miniServer.protocol.exception;


public class SocketUnavailableException  extends Exception{
    public SocketUnavailableException(){
        super();
    }
    public SocketUnavailableException(String message){
        super(message);
    }
    private static final long serialVersionUID = 2492127761542712979L;
}
