package com.aspire.nm.component.miniServer.plugin;

import com.aspire.nm.component.miniServer.protocol.Response;



public interface IfCacher extends Cacher<Response>{
    
    /**
     * @param time 过期时间,单位毫秒
     */
    public boolean put(String key,Response obj,int time);
    public Response get(String key);
}
