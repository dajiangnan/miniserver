package com.aspire.nm.component.miniServer.handle.exception;


public class NotFountException extends Exception {
    
    public NotFountException(){
        super();
    }
    public NotFountException(String message){
        super(message);
    }
    private static final long serialVersionUID = 2492923761542712979L;
}
