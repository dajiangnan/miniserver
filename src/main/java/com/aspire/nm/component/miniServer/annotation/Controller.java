package com.aspire.nm.component.miniServer.annotation;


import java.lang.annotation.Documented; 
import java.lang.annotation.ElementType; 
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy; 
import java.lang.annotation.Target; 
  
@Target(ElementType.METHOD) 
@Retention(RetentionPolicy.RUNTIME) 
@Documented 
public @interface Controller { 
	    int cacheTime() default 0;
		String desc() default "";  
	  String pathPattern() default "";
	  int timeOut() default 0;
      MethodType methodType() default MethodType.FOLLOW;
      public enum MethodType {  
            GET,POST,FOLLOW;
    	}
} 
