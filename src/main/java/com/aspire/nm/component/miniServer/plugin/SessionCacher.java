package com.aspire.nm.component.miniServer.plugin;

import java.util.Map;



public interface SessionCacher extends Cacher<Map<String,Object>>{

    public boolean put(String key,Map<String,Object> session,int time);
    public Map<String,Object> get(String key);
}
