package com.aspire.nm.component.miniServer.plugin.defaultImpl;

import java.util.Map;

import com.aspire.nm.component.util.CacheUtil;
import com.aspire.nm.component.miniServer.plugin.SessionCacher;


public class CommonSessionCacher implements SessionCacher {

    private CacheUtil cacheUtil;
    public CommonSessionCacher(){
        cacheUtil = new CacheUtil();
    }
    
    @Override
    public boolean put(String key, Map<String,Object> session, int time) {
        return cacheUtil.put(key, session, (long)time);
    }

    @SuppressWarnings("unchecked")
    @Override
    public Map<String,Object> get(String key) {
        return (Map<String,Object>)cacheUtil.get(key);
    }

    @Override
    public void start() {
    }

    @Override
    public void release() {
    }

}
