package com.aspire.nm.component.miniServer.handle;

public class ViewModel {
	
	
	public ViewModel(String viewTemplate){
		this.viewTemplate = viewTemplate;
	}
	
	public ViewModel(String viewTemplate,java.util.Map<?, ?> modelMap){
		this.viewTemplate = viewTemplate;
		this.modelMap = modelMap;
	}
	
	private java.util.Map<?, ?> modelMap;
	private String viewTemplate;
	
	public java.util.Map<?, ?> getModelMap() {
		return modelMap;
	}
	public void setModelMap(java.util.Map<?, ?> modelMap) {
		this.modelMap = modelMap;
	}
	public String getViewTemplate() {
		return viewTemplate;
	}
	public void setViewTemplate(String viewTemplate) {
		this.viewTemplate = viewTemplate;
	}
}
