package com.aspire.nm.component.miniServer.render.freeMaker;

import com.aspire.nm.component.miniServer.render.Render;
import com.aspire.nm.component.miniServer.render.RenderProvider;


public class FreeMakerRenderFactory implements RenderProvider {

    @Override
    public Render getRender() {
        return new FreeMakerRender();
    }

}
