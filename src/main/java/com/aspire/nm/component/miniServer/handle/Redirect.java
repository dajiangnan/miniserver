package com.aspire.nm.component.miniServer.handle;

public class Redirect {
	
	
	public Redirect(String url){
		this.url = url;
	}
	private String url;
    
    public String getUrl() {
        return url;
    }
    
    public void setUrl(String url) {
        this.url = url;
    }
	
}
