package com.aspire.nm.component.miniServer.render;


public interface Render {
    public String render(Object data,String source,String encode)throws RenderException;
}
