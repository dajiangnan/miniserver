package com.aspire.nm.component.util;


import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.DailyRollingFileAppender;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;

public class DynamicLogger
{
    private Map<String, Logger> loggerMap = new Hashtable();
    private Logger logger;
    private String dynamicKey;

    public DynamicLogger(Class<?> cls, String dynamicKey)
    {
        this.logger = Logger.getLogger(cls);
        this.dynamicKey = dynamicKey;
    }
    public DynamicLogger(String logName, String dynamicKey) {
        this.logger = Logger.getLogger(logName);
        this.dynamicKey = dynamicKey;
    }

    private Logger getLogger(String dynamicValue)
    {
        if (this.loggerMap.get(dynamicValue) == null)
        {
            synchronized (DynamicLogger.class)
            {
                if (this.loggerMap.get(dynamicValue) == null)
                {
                    Logger loggerDynamic = Logger.getLogger(this.logger.getName() + dynamicValue);

                    Enumeration e = this.logger.getAllAppenders();
                    while (e.hasMoreElements()) {
                        Object obj = e.nextElement();
                        if (obj instanceof DailyRollingFileAppender) {
                            DailyRollingFileAppender dailyRollingFileAppender = (DailyRollingFileAppender)obj;
                            DailyRollingFileAppender dailyRollingFileAppenderDynamic = new DailyRollingFileAppender();

                            dailyRollingFileAppenderDynamic.setThreshold(dailyRollingFileAppender.getThreshold());
                            dailyRollingFileAppenderDynamic.setName(dailyRollingFileAppender.getName() + dynamicValue);
                            dailyRollingFileAppenderDynamic.setAppend(dailyRollingFileAppender.getAppend());
                            dailyRollingFileAppenderDynamic.setFile(dailyRollingFileAppender.getFile().replaceAll(this.dynamicKey, dynamicValue));
                            dailyRollingFileAppenderDynamic.setDatePattern(dailyRollingFileAppender.getDatePattern());

                            PatternLayout patternLayoutDynamic = new PatternLayout();
                            patternLayoutDynamic.setConversionPattern(((PatternLayout)dailyRollingFileAppender.getLayout()).getConversionPattern());
                            dailyRollingFileAppenderDynamic.setLayout(patternLayoutDynamic);
                            dailyRollingFileAppenderDynamic.activateOptions();

                            loggerDynamic.addAppender(dailyRollingFileAppenderDynamic);
                        }
                    }
                    loggerDynamic.setAdditivity(this.logger.getAdditivity());
                    loggerDynamic.setLevel(this.logger.getLevel());

                    this.loggerMap.put(dynamicValue, loggerDynamic);
                }
            }
        }
        return (Logger)this.loggerMap.get(dynamicValue);
    }

    public void debug(Object message)
    {
        if (this.logger.isDebugEnabled())
            this.logger.debug(message);
    }

    public void info(Object message) {
        if (this.logger.isInfoEnabled())
            this.logger.info(message);
    }

    public void warn(Object message) {
        this.logger.warn(message);
    }
    public void error(Object message) {
        this.logger.error(message);
    }
    public void fatal(Object message) {
        this.logger.fatal(message);
    }

    public void debug(String dynamicValue, Object message)
    {
        if (this.logger.isDebugEnabled())
            if (StringUtils.isEmpty(dynamicValue))
                this.logger.debug(message);
            else
                getLogger(dynamicValue).debug(message);
    }

    public void info(String dynamicValue, Object message)
    {
        if (this.logger.isInfoEnabled())
            if (StringUtils.isEmpty(dynamicValue))
                this.logger.info(message);
            else
                getLogger(dynamicValue).info(message);
    }

    public void warn(String dynamicValue, Object message)
    {
        if (StringUtils.isEmpty(dynamicValue))
            this.logger.warn(message);
        else
            getLogger(dynamicValue).warn(message);
    }

    public void error(String dynamicValue, Object message)
    {
        if (StringUtils.isEmpty(dynamicValue))
            this.logger.error(message);
        else
            getLogger(dynamicValue).error(message);
    }

    public void fatal(String dynamicValue, Object message) {
        if (StringUtils.isEmpty(dynamicValue))
            this.logger.error(message);
        else
            getLogger(dynamicValue).fatal(message);
    }
}