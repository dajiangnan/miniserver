package com.aspire.nm.component.util;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class ReflectUtil {

    public static String getReturnTypePrimitiveContent(Method method, Object obj) {

        if (method.getReturnType().isPrimitive())
            return String.valueOf(obj);

        if (method.getReturnType().equals(String.class)) {
            return ((String) obj);
        }
        if (method.getReturnType().equals(Integer.class)) {
            return ((Integer) obj).toString();
        }
        if (method.getReturnType().equals(Byte.class)) {
            return ((Byte) obj).toString();
        }
        if (method.getReturnType().equals(Long.class)) {
            return ((Long) obj).toString();
        }
        if (method.getReturnType().equals(Double.class)) {
            return ((Double) obj).toString();
        }
        if (method.getReturnType().equals(Float.class)) {
            return ((Float) obj).toString();
        }
        if (method.getReturnType().equals(Character.class)) {
            return ((Character) obj).toString();
        }
        if (method.getReturnType().equals(Short.class)) {
            return ((Short) obj).toString();
        }
        if (method.getReturnType().equals(java.math.BigDecimal.class)) {
            return ((java.math.BigDecimal) obj).toString();
        }
        if (method.getReturnType().equals(java.math.BigInteger.class)) {
            return ((java.math.BigInteger) obj).toString();
        }
        if (method.getReturnType().equals(Boolean.class)) {
            return ((Boolean) obj).toString();
        }
        if (method.getReturnType().equals(java.util.Date.class)) {
            return ((java.util.Date) obj).toString();
        }
        return null;
    }


}
